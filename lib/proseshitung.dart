import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HitungVolumeKubus extends StatefulWidget {
  @override
  _HitungVolumeKubusState createState() => _HitungVolumeKubusState();
}

class _HitungVolumeKubusState extends State<HitungVolumeKubus> {
  double sisi1=0;
  double sisi2=0;
  double sisi3=0;
  double volume=0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Aplikasi Menghitung Volume Kubus", style: TextStyle(color: Colors.white),),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Color(0xff004d40), Color(0xff4db6ac)],
                  begin: FractionalOffset.topLeft,
                  end: FractionalOffset.bottomCenter,
            )
          ),
        ),
      ),

      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              width: double.infinity,
              height: 200,
              margin: EdgeInsets.all(16),
              padding: EdgeInsets.all(16),
              color: Colors.tealAccent,
              child: Image.asset("assets/kubus.png", fit: BoxFit.contain,),
          ),
          Text("Hitung Volume Kubus", style: TextStyle(fontWeight: FontWeight.bold),),
          Row(
            children: [
              Text("Sisi Rusuk (r1)"),
              Expanded(
                child: TextField(
                  onChanged: (txt){
                    setState(() {
                      sisi1= double.parse(txt);
                    });
                  },
                  keyboardType: TextInputType.number,
                  maxLength: 5,
                ),
              )
            ],
          ),
          Row(
            children: [
              Text("Sisi Rusuk (r2)"),
              Expanded(
                child: TextField(
                  onChanged: (txt){
                    setState(() {
                      sisi2= double.parse(txt);
                    });
                  },
                  keyboardType: TextInputType.number,
                  maxLength: 5,
                ),
              )
            ],
          ),
          Row(
            children: [
              Text("Sisi Rusuk (r3)"),
              Expanded(
                child: TextField(
                  onChanged: (txt){
                    setState(() {
                      sisi3= double.parse(txt);
                    });
                  },
                  keyboardType: TextInputType.number,
                  maxLength: 5,
                ),
              )
            ],
          ),
          // ignore: deprecated_member_use
          RaisedButton(
              onPressed: (){
                setState(() {
                  volume=  sisi1 * sisi2 * sisi3;
                });
              },
              child: Text("Mulai Hitung"),
          ),
        
          Text("Volume Kubus Tersebut Adalah=$volume",style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),)
        ],
      ),
    );
  }
}